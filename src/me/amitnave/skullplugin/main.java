package me.amitnave.skullplugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;

public class main extends JavaPlugin implements Listener {
	FileConfiguration config = getConfig();
	List<String> skulls = new ArrayList<String>();
	public static HashMap<Location, String> SkullList = new HashMap<Location, String>();

	@Override
	public void onEnable() {
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "SkullPlugin has been loaded!");
		Bukkit.getServer().getPluginManager().registerEvents(new EventListener(), this);
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		skulls = config.getStringList("skullist");
		for (String data : skulls) {
			String[] dataArray = data.split(":");
			String[] xyz = dataArray[0].split(",");
			Integer x = Integer.parseInt(xyz[0]);
			Integer y = Integer.parseInt(xyz[1]);
			Integer z = Integer.parseInt(xyz[2]);
			String dm = dataArray[1];
			String n = dataArray[2];
			String w = dataArray[3];
			Location loc = new Location(Bukkit.getWorld(w), x, y, z);
			SkullList.put(loc, dm + ":" + n);
		}
		// get the configuration, write to skull list the location from xyz and the
		// death message and the name

	}

	@Override
	public void onDisable() {

		Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "SkullPlugin has been disabled!");
		// get skullList, save the location as x:y:z and the death message and name as
		// dm:n

		config.set("skullist", skulls);
		saveConfig();
		this.saveDefaultConfig();
	}

	@EventHandler
	public void onBlockPlaceEvent(BlockPlaceEvent e) {
		// if block is skull, write location and metadata to config file
		if (e.getBlock().getType() == Material.SKULL) {
			String dm = "";
			Location l = e.getBlock().getLocation();
			if (e.getItemInHand().getItemMeta().hasLore()) {
				dm = e.getItemInHand().getItemMeta().getLore().get(0);
				ItemMeta im = e.getItemInHand().getItemMeta();
				SkullMeta sm = (SkullMeta) im;
				String name = sm.getOwningPlayer().getName();
				String w = l.getWorld().getWorldFolder().getName();
				String data = l.getBlockX() + "," + l.getBlockY() + "," + l.getBlockZ() + ":" + dm + ":" + name + ":"
						+ w;
				skulls.add(data);
				SkullList.put(l, dm + ":" + name);
			} else {
				return;
			}

		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onBlockBreakEvent(BlockBreakEvent e) {
		if (e.getBlock().getType() == Material.SKULL) {
			if (SkullList.get(e.getBlock().getLocation()) != null) {
				String data = SkullList.get(e.getBlock().getLocation());

				String[] datalist = data.split(":");
				String dm = datalist[0];
				String n = datalist[1];
				ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);

				ItemMeta im = skull.getItemMeta();

				im.setDisplayName(ChatColor.RESET + n + "'s Skull");

				ArrayList<String> lore = new ArrayList<String>();
				lore.add(dm);
				im.setLore(lore);
				SkullMeta sm = (SkullMeta) im;

	sm.setOwningPlayer(Bukkit.getOfflinePlayer(n));

				skull.setItemMeta(sm);
				e.getBlock().setType(Material.AIR);
				e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), skull);
				e.setCancelled(true);
				if (SkullList.get(e.getBlock().getLocation()) != null) {
					SkullList.remove(e.getBlock().getLocation());
				}
				Location l = e.getBlock().getLocation();
				String w = e.getBlock().getWorld().getWorldFolder().getName();
				String skullsdata = l.getBlockX() + "," + l.getBlockY() + "," + l.getBlockZ() + ":" + dm + ":" + n + ":"
						+ w;
				skulls.remove(skullsdata);
			}

		}
	}
}
