package me.amitnave.skullplugin;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import net.md_5.bungee.api.ChatColor;

public class EventListener implements Listener {
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		Player p = e.getEntity().getPlayer();
		String name = p.getName();

		String dm = e.getDeathMessage();

		ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);

		ItemMeta im = skull.getItemMeta();

		im.setDisplayName(ChatColor.RESET + name + "'s Skull");

		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.RESET + "" + ChatColor.GRAY + dm);
		im.setLore(lore);
		SkullMeta sm = (SkullMeta) im;

		sm.setOwningPlayer(p);

		skull.setItemMeta(sm);
		if (e.getEntity().getKiller() instanceof Player) {
			if (e.getEntity().hasPermission("skullplugin.behead")) {
				e.getDrops().add(skull);
			}
		}
	}

	@EventHandler
	public void onPlayerRightClick(PlayerInteractEvent e) {
		e.getHand();
		if (e.getHand() == EquipmentSlot.HAND) {
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (e.getClickedBlock().getType() == Material.SKULL) {
					if (main.SkullList.get(e.getClickedBlock().getLocation()) != null) {

						String n = main.SkullList.get(e.getClickedBlock().getLocation()).split(":")[1];
						String dm = main.SkullList.get(e.getClickedBlock().getLocation()).split(":")[0];
						e.getPlayer().sendMessage(ChatColor.GRAY + "This is " + ChatColor.stripColor(n) + ". " + dm);
					}
				}
			}
		}
	}

}
